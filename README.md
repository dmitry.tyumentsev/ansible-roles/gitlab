Gitlab role
=========

Installs Gitlab on Debian/Ubuntu servers.

Example Playbook
----------------
```yml
- hosts: all
  become: true
  roles:
    - tyumentsev4.gitlab
```
